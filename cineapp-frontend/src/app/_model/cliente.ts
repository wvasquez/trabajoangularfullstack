export class Cliente {
    idCliente: number;
    nombres: string;
    apellidos: string;
    fechaNac: Date;
    dni: string;
    correo: string;
    
     _foto: any;
    _isFoto: boolean;
}